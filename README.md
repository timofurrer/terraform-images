# Terraform Images

**This product is not officially supported by GitLab. We provide it on a community support bases to allow Terraform users to continue using Terraform with GitLab.**

> 🚨 This repository won't upgrade to any new Terraform releases with the BSL license.
> Please follow [this issue](https://gitlab.com/gitlab-org/terraform-images/-/issues/114) for updates.
> 
> We'd like to advise you to use the new [**OpenTofu CI/CD component**](https://gitlab.com/components/opentofu).
>
> Alternatively, you may build this project with the latest Terraform release yourself and 
> host it in your own registry.
>
> The OpenTofu support in this repository has been removed in favor of the
> aforementioned new [OpenTofu CI/CD component](https://gitlab.com/components/opentofu).


This repository provides a docker image which contains the `gitlab-terraform` shell script. This script is a thin wrapper around the `terraform` binary. Its main purpose is to serve the [Infrastructure as code with Terraform and GitLab
](https://docs.gitlab.com/ee/user/infrastructure/), by extracting some of the standard configuration a user would need to set up to use the Terraform backend on GitLab as well as the Terraform merge request widget.

## Build and host the image and template yourself for up-to-date Terraform

Due to the HashiCorp Terraform license change to BSL, GitLab is no longer legally allowed 
to distribute Terraform as part of its products. 
Therefore, this project and the associated Terraform CI/CD templates are deprecated and will
not receive any updates. The following guide explains how you can host and build 
a container image containing an up-to-date Terraform version and the `gitlab-terraform` script and
deploy it alongside the Terraform CI/CD templates in your own GitLab project.

The following steps need to be performed:

1. Mirror this project using a [Pull Mirror](https://docs.gitlab.com/ee/user/project/repository/mirror/pull.html) (with GitLab self-managed you cannot fork from GitLab.com).
    - Alternatively, you may use a fork on GitLab.com to control the updates from this canonical repository.
    - or you use a fork in combination with the pull mirror, so that the pull mirror on the self-managed instance is always
      up-to-date and you use a fork from that to control when to roll-out the updates.
1. Change the `.terraform-versions` matrix in the `.gitlab-ci.yml` file to the Terraform versions you want to build
1. Tag the project to trigger a release pipeline. Make sure it succeeds.
1. Include the template in a project on your instance:

```yaml
include:
  - project: "<path-to-your-mirror-project>"
    file: "/templates/Terraform.gitlab-ci.yml"

default:
  image:
    # The image name is required. The template doesn't set it.
    name: $CI_REGISTRY_HOST/<path-to-your-mirror-project>/releases/<Terraform-version>:<project-version>"
```

***

## How to use it

### Required Environment Variables

The wrapper expects three environment variables to be set:

#### `TF_ADDRESS`

Should be the backend URL. For the GitLab backend it will be something like:

`"<GITLAB_API_URL>/projects/<PROJECT_ID>/terraform/state/<STATE_NAME>"`

- `GITLAB_API_URL` is the URL of your GitLab API (you can use `$CI_API_V4_URL` in [GitLab CI/CD](https://docs.gitlab.com/ee/ci/variables/index.html)).
- `PROJECT_ID` is the id of the project you're using as your infrastructure as code (you can use `$CI_PROJECT_ID` in [GitLab CI/CD](https://docs.gitlab.com/ee/ci/variables/index.html))
- `STATE_NAME` can be arbitrarily defined to the Terraform state name that you create.

#### `TF_USERNAME`

Is your user login name, which must have maintainer access. If this is unset, it will default to the value of `GITLAB_USER_LOGIN` which is the username that triggered the build.

#### `TF_PASSWORD`

An access token created for the above maintainer with the `api` scope. If this is unset, it will default to the value of `CI_JOB_TOKEN` and override the `TF_USERNAME` to match.

### Support for GitLab CI Environment Variables

`gitlab-terraform` exposes the following GitLab CI Environment Variables as `TF_VAR` inputs

- `CI_JOB_ID`

- `CI_COMMIT_SHA`

- `TF_VAR_CI_JOB_STAGE`

- `CI_PROJECT_ID`

- `CI_PROJECT_NAME`

- `CI_PROJECT_NAMESPACE`

- `CI_PROJECT_PATH`

- `CI_PROJECT_URL`

You can use these in your Terraform files in the following way

```
variable "CI_PROJECT_NAME" {
  type    = string
}
```

### Terraform lockfile handling

If you commit the `.terraform.lock.hcl` file to your repository we recommend setting `TF_INIT_FLAGS` to `-lockfile=readonly` to prevent changes to the lockfile.
